package com.fjlt.dao;

import com.fjlt.entity.Solution;
import com.fjlt.entity.Solution_Details;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SolutionMapper {
    public Solution_Details SolutionFinderById(Integer id);
    public List<Solution> allSolutionProducts();
}
