package com.fjlt.dao;

import com.fjlt.entity.Home;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface HomeMapper {
    public Home homeInfo();
}
