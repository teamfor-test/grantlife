package com.fjlt.dao;

import com.fjlt.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProductMapper {
    public Product getProductById(Integer id);
    public List<Product> allProducts();
}
