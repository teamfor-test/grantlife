package com.fjlt.controller;

import com.fjlt.service.FileService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
public class FileController {

	@Autowired
	FileService fileService;

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////前端相关的文件处理///////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	//接收前端请求上传文件
	@ResponseBody
	@ApiOperation(value = "接收前端请求上传文件", notes = "上传MultipartFile数据（未存储进数据库）", code = 200, produces = "application/json")
	@RequestMapping(value = "/uploadImg/", method = RequestMethod.PUT)
	public String uploadImg( @ApiParam(value="上传文件",required=true) MultipartFile file) {
		try {
			fileService.upload("/var/www/html", file);
		}catch (Exception e){
			e.printStackTrace();
		}
		return "true";
	}

	//接收前端请求下载文件
	@ResponseBody
	@ApiOperation(value = "接收前端请求下载文件", notes = "根据文件名从数据库中查询到路径，返回url（未完成。还未调用数据库接口）", code = 200, produces = "application/json")
	@RequestMapping(value = "/downloadImg/{fileName}", method = RequestMethod.POST)
	public String downloadImg(@PathVariable("fileName") String fileName) {
		try {
			return fileName;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("false");
			return "false";
		}
	}
}
