package com.fjlt.controller;

import com.fjlt.entity.Home;
import com.fjlt.service.HomeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class HomeController {
    @Autowired
    HomeService homeService;

    @GetMapping(value = "/homeInfo")
    @ApiOperation(value = "默认主页信息",notes = "默认主页",code = 200,produces = "application/json")
    public Home homeInfo(){
        Home home=homeService.getHomeInfo();
        if (home==null){
            System.out.println("Error");
            return null;
        }else {
            System.out.println("Getdata");
            return home;
        }
    }
}
