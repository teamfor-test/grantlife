package com.fjlt.controller;

import com.fjlt.entity.Product;
import com.fjlt.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping(value = "/getProduct")
    @ApiOperation(value = "测试接口，",notes = "测试接口，默认返回id=1的产品",code = 200,produces = "application/json")
    public Product getProduct(){
        Product product = productService.getProduct();
        if (product == null){
            System.out.println("Null");
            return null;
        }else {
            System.out.println("getData");
            return product;
        }
    }
    @GetMapping(value = "/allProduct")
    @ApiOperation(value = "查询所有产品",notes = "所有产品的全部信息",code = 200,produces = "application/json")
    public List<Product> allProduct(){
        List<Product> product=productService.allProducts();
        return product;
    }
    @RequestMapping(value = "getById",method = RequestMethod.GET)
    @ApiOperation(value = "通过ID查询产品",notes = "查询ID并返回该ID产品的全部信息",code = 200,produces = "application/json")
    public Product getById(@RequestParam Integer id){
        Product product=productService.getById(id);
        return product;
    }
}
