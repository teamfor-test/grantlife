package com.fjlt.controller;

import com.fjlt.entity.Product;
import com.fjlt.entity.Solution_Details;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.stereotype.Controller;

import java.net.HttpURLConnection;
import java.io.IOException;
import java.net.URL;

import com.fjlt.entity.Solution;
import com.fjlt.service.SolutionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
@RestController
public class SolutionController {

	@Autowired
	SolutionService solutionService;

		@RequestMapping(value="/solutionHome",method = RequestMethod.GET,produces = {"application/json;charset=UTF-8"})
		public  List<Solution> getSingleSolution(){
			List<Solution> solution = solutionService.allSolutions();
			if(solution == null)
				return null;
			return solution;
		}

		@RequestMapping(value="/solutionDetail",method = RequestMethod.GET,produces = {"application/json;charset=UTF-8"})
		public Solution_Details getSolutionDetail(@RequestParam Integer id){
			Solution_Details solution = solutionService.getById(id);
			if(solution == null)
				return null;
			return solution;
		}
}
