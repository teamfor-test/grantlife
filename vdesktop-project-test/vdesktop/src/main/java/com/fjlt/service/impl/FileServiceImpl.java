package com.fjlt.service.impl;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.fjlt.entity.SFTP;
import com.fjlt.service.FileService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;

import static com.fjlt.entity.SFTPUtil.*;


@Service
public class FileServiceImpl implements FileService {

    /**
     * 上传从前端接收的文件
     * @param path 上传的目录-相对于SFPT设置的用户访问目录，
     * 为空则在SFTP设置的根目录进行创建文件（除设置了服务器全磁盘访问）
     * @param file 要上传的文件
     */
    @Override
    public void upload(String path, MultipartFile file ) throws Exception {
        if (file != null) {
            // 获取文件的后缀
            String originalName = file.getOriginalFilename();
            String suffix = originalName.substring(originalName.lastIndexOf("."));
            // 时间戳+随机数生成文件名
            String timeName = String.valueOf(System.currentTimeMillis()) + (int) ((Math.random() * 9 + 1) * 100000) + suffix;

            // 将文件转化为字节流
            InputStream is = file.getInputStream();

            SFTP s = new SFTP();
            getConnect(s);//建立连接
            Session session = s.getSession();
            Channel channel = s.getChannel();
            ChannelSftp sftp = s.getSftp();// sftp操作类
            try {
                try {
                    sftp.cd(path); //进入目录
                } catch (SftpException sException) {
                    if (sftp.SSH_FX_NO_SUCH_FILE == sException.id) { //指定上传路径不存在
                        sftp.mkdir(path);//创建目录
                        sftp.cd(path);  //进入目录
                    }
                }
                sftp.put(is, originalName);
                is.close();

            } catch (Exception e) {
                throw new Exception(e.getMessage(), e);
            } finally {
                disConn(session, channel, sftp);
            }
        }
    }

    /**
     * 上传本地文件
     * @param path 上传的目录-相对于SFPT设置的用户访问目录，
     * 为空则在SFTP设置的根目录进行创建文件（除设置了服务器全磁盘访问）
     * @param uploadFile 要上传的文件全路径
     */
    @Override
    public void upload(String path, String uploadFile) throws Exception {

        SFTP s=new SFTP();
        getConnect(s);//建立连接
        Session session = s.getSession();
        Channel channel = s.getChannel();
        ChannelSftp sftp = s.getSftp();// sftp操作类
        try {
            try{
                sftp.cd(path); //进入目录
            }catch(SftpException sException){
                if(sftp.SSH_FX_NO_SUCH_FILE == sException.id){ //指定上传路径不存在
                    sftp.mkdir(path);//创建目录
                    sftp.cd(path);  //进入目录
                }
            }
            File file = new File(uploadFile);
            InputStream in= new FileInputStream(file);
            sftp.put(in, file.getName());
            in.close();

        } catch (Exception e) {
            throw new Exception(e.getMessage(),e);
        } finally {
            disConn(session,channel,sftp);
        }
    }




///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 下载文件
     * @param sourcePath 下载目录 根据SFTP设置的根目录来进行传入
     * @param sourceFileName  下载的文件
     * @param targetPath  存在本地的路径
     * @param targetFileName 保存的文件名
     * */
    @Override
    public void download(String sourcePath, String sourceFileName, String targetPath, String targetFileName) throws Exception {
        SFTP s=new SFTP();
        getConnect(s);//建立连接
        Session session = s.getSession();
        Channel channel = s.getChannel();
        ChannelSftp sftp = s.getSftp();// sftp操作类
        try {

            sftp.cd(sourcePath); //进入目录
            File file = new File(targetPath);
            boolean bFile = file.exists();
            if (!bFile) {
                bFile = file.mkdirs();//创建目录
            }
            OutputStream out=new FileOutputStream(new File(targetPath, targetFileName));

            sftp.get(sourceFileName, out);

            out.flush();
            out.close();

        } catch (Exception e) {
            throw new Exception(e.getMessage(),e);
        } finally {
            disConn(session,channel,sftp);
        }
    }

}
