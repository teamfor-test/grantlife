package com.fjlt.service.impl;

import com.fjlt.dao.HomeMapper;
import com.fjlt.entity.Home;
import com.fjlt.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HomeServiceImpl implements HomeService {
    @Autowired
    HomeMapper homeMapper;
    @Override
    public Home getHomeInfo(){
        Home home=homeMapper.homeInfo();
        return home;
    }

}
