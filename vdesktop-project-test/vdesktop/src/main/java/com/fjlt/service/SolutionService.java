package com.fjlt.service;

import com.fjlt.entity.Solution;
import com.fjlt.entity.Solution_Details;

import java.util.List;

public interface SolutionService {
    public List<Solution> allSolutions();
    public Solution_Details getById(Integer id);
}
