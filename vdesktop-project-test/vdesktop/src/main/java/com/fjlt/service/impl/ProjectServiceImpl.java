package com.fjlt.service.impl;

import com.fjlt.dao.ProductMapper;
import com.fjlt.entity.Product;
import com.fjlt.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProductService {
    @Autowired
    public ProductMapper productMapper;

    @Override
    public Product getProduct(){
        Product product = productMapper.getProductById(1);
        return product;
    }
    public List<Product> allProducts(){
        List<Product> product=productMapper.allProducts();
        return product;
    }
    public Product getById(Integer id){
        Product product = productMapper.getProductById(id);
        return product;
    }

}
