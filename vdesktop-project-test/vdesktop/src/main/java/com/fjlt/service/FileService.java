package com.fjlt.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    void upload(String path, MultipartFile file) throws Exception;
    void upload(String path, String uploadFile) throws Exception;
    void download(String sourcePath, String sourceFileName, String targetPath, String targetFileName) throws Exception;
}
