//impl写业务逻辑，service创建接口给controller调用（与前端交互）

package com.fjlt.service;

import com.fjlt.entity.Home;

public interface HomeService {
    public Home getHomeInfo();
    //public Home selectHomeById();
}
