package com.fjlt.service.impl;

import com.fjlt.dao.SolutionMapper;
import com.fjlt.entity.Solution;
import com.fjlt.entity.Solution_Details;
import com.fjlt.service.SolutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolutionServiceImpl implements SolutionService {
    @Autowired
    public SolutionMapper solutionMapper;

    @Override
    public List<Solution> allSolutions(){
        List<Solution> product=solutionMapper.allSolutionProducts();
        return product;
    }

    public Solution_Details getById(Integer id){
        Solution_Details product = solutionMapper.SolutionFinderById(id);
        return product;
    }

}
