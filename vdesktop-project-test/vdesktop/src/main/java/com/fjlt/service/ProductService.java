package com.fjlt.service;

import com.fjlt.entity.Product;

import java.util.List;


public interface ProductService {
    public Product getProduct();
    public List<Product> allProducts();
    public Product getById(Integer id);
}
