package com.fjlt.entity;

public class Solution_Details{
    private Integer product_id;
    private String product_theme;
    private String product_img_src;
    private String product_content;

    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public String getProduct_theme() {
        return product_theme;
    }

    public void setProduct_theme(String product_theme) {
        this.product_theme = product_theme;
    }

    public String getProduct_img_src() {
        return product_img_src;
    }

    public void setProduct_img_src(String product_img_src) {
        this.product_img_src = product_img_src;
    }

    public String getProduct_content() {
        return product_content;
    }

    public void setProduct_content(String product_content) {
        this.product_content = product_content;
    }

    @Override
    public String toString() {
        return "Solution_Details{" +
                "product_id=" + product_id +
                ", product_theme='" + product_theme + '\'' +
                ", product_img_src='" + product_img_src + '\'' +
                ", product_content='" + product_content + '\'' +
                '}';
    }
}
