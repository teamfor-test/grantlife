package com.fjlt.entity;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.util.Properties;

public class SFTPUtil {

    /**
     * 连接ftp/sftp服务器
     */
    public static void getConnect(SFTP s) throws Exception {

        /** 密钥的密码  */
//      String privateKey ="key";
//        /** 密钥文件路径  */
//      String passphrase ="path";
        /** 主机 */
        String host ="10.206.143.99";
        /** 端口 */
        int port =22;
        /** 用户名 */
        String username ="zzz";
        /** 密码 */
        String password ="12345678";

        Session session = null;
        Channel channel = null;
        ChannelSftp sftp = null;// sftp操作类

        JSch jsch = new JSch();


        //设置密钥和密码
        //支持密钥的方式登陆，只需在jsch.getSession之前设置一下密钥的相关信息就可以了
//      if (privateKey != null && !"".equals(privateKey)) {
//             if (passphrase != null && "".equals(passphrase)) {
//              //设置带口令的密钥
//                 jsch.addIdentity(privateKey, passphrase);
//             } else {
//              //设置不带口令的密钥
//                 jsch.addIdentity(privateKey);
//             }
//      }
        session = jsch.getSession(username, host, port);
        session.setPassword(password);
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no"); // 不验证 HostKey
        session.setConfig(config);
        try {
            session.connect();
        } catch (Exception e) {
            if (session.isConnected())
                session.disconnect();
            System.out.println("连接服务器失败,请检查主机[" + host + "],端口[" + port
                    + "],用户名[" + username + "],端口[" + port
                    + "]是否正确,以上信息正确的情况下请检查网络连接是否正常或者请求被防火墙拒绝.");
        }
        channel = session.openChannel("sftp");
        try {
            channel.connect();
        } catch (Exception e) {
            if (channel.isConnected())
                channel.disconnect();
            System.out.println("连接服务器失败,请检查主机[" + host + "],端口[" + port
                    + "],用户名[" + username + "],密码是否正确,以上信息正确的情况下请检查网络连接是否正常或者请求被防火墙拒绝.");
        }
        sftp = (ChannelSftp) channel;

        s.setChannel(channel);
        s.setSession(session);
        s.setSftp(sftp);

    }

    /**
     * 断开连接

     */
    public static void disConn(Session session,Channel channel,ChannelSftp sftp)throws Exception{
        if(null != sftp){
            sftp.disconnect();
            sftp.exit();
            sftp = null;
        }
        if(null != channel){
            channel.disconnect();
            channel = null;
        }
        if(null != session){
            session.disconnect();
            session = null;
        }
    }
}
