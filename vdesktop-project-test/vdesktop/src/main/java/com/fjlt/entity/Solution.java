package com.fjlt.entity;

import io.swagger.models.auth.In;

public class Solution {
    private Integer id;
    private String image_src;
    private String description;
    private String title;
    private Integer detail_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage_src() {
        return image_src;
    }

    public void setImage_src(String image_src) {
        this.image_src = image_src;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDetail_id() {
        return detail_id;
    }

    public void setDetail_id(Integer detail_id) {
        this.detail_id = detail_id;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "id=" + id +
                ", image_src='" + image_src + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", detail_id=" + detail_id +
                '}';
    }
}



