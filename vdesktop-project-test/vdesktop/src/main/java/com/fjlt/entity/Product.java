package com.fjlt.entity;

public class Product {
    private Integer id;
    private String image_src;
    private String description;
    private String title;

    public Integer getId(){
        return this.id;
    }
    public void setId(Integer id_new){
        this.id=id_new;
    }

    public String getImage_src(){
        return this.image_src;
    }
    public void setImage_src(String img_src_new){
        this.image_src=img_src_new;
    }

    public String getDescription(){
        return this.description;
    }
    public void setDescription(String description_new){
        this.description=description_new;
    }

    public String getTitle(){
        return this.title;
    }
    public void setTitle(String title_new){
        this.title=title_new;
    }

    @Override
    public String toString(){
        return "Product{" +
                "id=" +id+","+
                "image_src=" +image_src+","+
                "description="+description+","+
                "title="+title+","+
                "}";
    }
}
